<?php

/**
 * Drupal Commerce Nelnet Integration
 *
 * @author    Tech Services, Student Life <tsadmin@umich.edu>
 * @developer Ryan VanDaele <rvandael@umich.edu>
 * @copyright 2012, The Regents of the University of Michigan
 * @link      http://studentlife.umich.edu
 */


/**
 * DSA_Commerce_Nelnet_Request class.  Class to handle building Nelnet requests
 */
class DSA_Commerce_Nelnet_Request {
  private $parameters;
  
  /**
   * __construct function.  Constructor
   * 
   * @access public
   * @return void
   */
  public function __construct() {
    
  }
  
  
  /**
   * Add a parameter to the request
   * 
   * @access public
   * @param String $parameter
   * @param mixed $value
   * @return void
   */
  public function addParameter($parameter,$value) {
    $parameter_defs=array(
      'orderNumber'=>32,
      'orderType'=>32,
      'orderDescription'=>32,
      'amountDue'=>32,
      'paymentMethod'=>6,
      'streetOne'=>50,
      'streetTwo'=>50,
      'city'=>20,
      'state'=>2,
      'zip'=>10,
      'country'=>20,
      'daytimePhone'=>20,
      'eveningPhone'=>20,
      'email'=>50,
      'userChoice1'=>50,
      'userChoice2'=>50,
      'userChoice3'=>50,
      'userChoice4'=>50,
      'userChoice5'=>50,
      'userChoice6'=>50,
      'userChoice7'=>50,
      'userChoice8'=>50,
      'userChoice9'=>50,
      'userChoice10'=>50,
      'redirectUrl'=>0,
      'redirectUrlParameters'=>0,
      'retriesAllowed'=>9,
      'timestamp'=>0,
      'hash'=>0);
    
    if (array_key_exists($parameter,$parameter_defs)) {
      if ($parameter_defs[$parameter]>0) {
        $value=substr($value,0,$parameter_defs[$parameter]);
      }
      
      $this->parameters[$parameter]=$value;
    }
  }
  
  
  /**
   * Generate the hash for the current parameters
   * 
   * @access public
   * @param String $sharedSecret
   * @return String
   */
  public function generateHash($sharedSecret,$mode) {
    $hash_string='';
    foreach ($this->parameters as $key=>$value) {
      $hash_string.=$value;
    }
    
    $hash_string.=$sharedSecret;
    
    if ($mode=='sha256')
      $hash_string=hash('sha256',$hash_string);
    else
      $hash_string=hash('md5',$hash_string);
    
    return $hash_string;
  }
  
  
  /**
   * Generate a timestamp in milliseconds for Nelnet
   * 
   * @access public
   * @return int
   */
  public function getTimestamp() {
    return number_format(round(microtime(true) * 1000), 0, '.', '');
  }
  
  
  /**
   * Add parameters to an already created drupal form
   * 
   * @access public
   * @param mixed &$form
   * @return void
   */
  public function addParametersToForm(&$form) {
    foreach ($this->parameters as $key=>$value) {
      $form[$key] = array('#type' => 'hidden', '#value' => $value);
    }
  }
}