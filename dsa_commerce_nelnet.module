<?php

/**
 * Drupal Commerce Nelnet Integration
 *
 * @author    Tech Services, Student Life <tsadmin@umich.edu>
 * @developer Ryan VanDaele <rvandael@umich.edu>
 * @copyright 2012, The Regents of the University of Michigan
 * @link      http://studentlife.umich.edu
 */

/**
 * dsa_commerce_nelnet_menu function.  Implements hook_menu().
 * 
 * @access public
 * @return array
 */
function dsa_commerce_nelnet_menu() {
  $items['checkout/nelnet/return'] = array(
    'title' => t('Order return'),
    'page callback' => 'dsa_commerce_nelnet_order_return',
    'access arguments' => array('access checkout'),
    'type' => MENU_CALLBACK,
  );
  
  return $items;
}

/**
 * dsa_commerce_nelnet_commerce_payment_method_info function.  Implements hook_commerce_payment_method_info().
 * 
 * @access public
 * @return array
 */
function dsa_commerce_nelnet_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_payment_nelnet'] = array(
  	'base' => 'dsa_commerce_nelnet',
    'title' => t('Nelnet'),
    'description' => t('Nelnet authorization processing.'),
    'active' => TRUE,
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * dsa_commerce_nelnet_commerce_checkout_page_info_alter function.  Change buttons so user knows where they are in the process
 * 
 * @access public
 * @param mixed &$pages
 * @return void
 */
function dsa_commerce_nelnet_commerce_checkout_page_info_alter(&$pages) {
  if(!empty($pages['review'])) {
    $pages['review']['submit_value'] = t('Complete Purchase');
  } 
  
  if(!empty($pages['checkout'])) {
    $pages['checkout']['submit_value'] = t('Review Order');
  }
}

/**
 * dsa_commerce_nelnet_settings_form function.  Implements hook_settings_form().
 * 
 * @access public
 * @param mixed $settings (default: NULL)
 * @return void
 */
function dsa_commerce_nelnet_settings_form($settings = NULL) {
  $form = array();
  
  // Default settings handling
  if (is_string($settings)) {
    $settings = array();
  }
  
  if ($settings==array()) {
    $settings['commerce_nelnet_key'] = 'key';
    $settings['commerce_nelnet_test_url'] = '';
    $settings['commerce_nelnet_live_url'] = '';
    $settings['commerce_nelnet_debug'] = '0';
    $settings['commerce_nelnet_hash_type'] = 'md5';
    $settings['commerce_nelnet_order_type'] = '';
    $settings['commerce_nelnet_order_description'] = '';
  }
  
    $form['commerce_nelnet_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Shared Secret Key'),
      '#default_value' => $settings['commerce_nelnet_key'],
      '#required' => TRUE,
    );
    
    $form['commerce_nelnet_test_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Test URL'),
      '#default_value' => $settings['commerce_nelnet_test_url'],
      '#required' => TRUE,
    );
    
    $form['commerce_nelnet_live_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Live URL'),
      '#default_value' => $settings['commerce_nelnet_live_url'],
      '#required' => TRUE,
    );
    
    $form['commerce_nelnet_debug'] = array(
      '#title' => 'Debug Mode (Live vs Test URL)',
      '#type' => 'radios',
      '#options' => array(
        0 => t('Test URL') . ' - ' . $settings['commerce_nelnet_test_url'],
        1 => t('Live URL') . ' - ' . $settings['commerce_nelnet_live_url'],
      ),
      '#default_value' => $settings['commerce_nelnet_debug'],
      '#required' => TRUE,
    );
    
    $form['commerce_nelnet_hash_type'] = array(
      '#type' => 'radios',
      '#title' => t('Hashing Method'),
      '#options' => array(
        'md5' => t('MD5'),
        'sha256' => t('SHA256'),
      ),
      '#default_value' => $settings['commerce_nelnet_hash_type'],
      '#required' => TRUE,
    );
    
    $form['commerce_nelnet_order_type'] = array(
      '#type' => 'textfield',
      '#title' => t('Order Type'),
      '#default_value' => $settings['commerce_nelnet_order_type'],
      '#required' => TRUE,
    );
    
    $form['commerce_nelnet_order_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Order Description'),
      '#default_value' => $settings['commerce_nelnet_order_description'],
      '#required' => FALSE,
    );
  return $form;
}

/**
 * dsa_commerce_nelnet_redirect_form function.  Commerce redirect form callback.  Redirect over to nelnet
 * 
 * @access public
 * @param mixed $form
 * @param mixed &$form_state
 * @param mixed $order
 * @param mixed $payment_method
 * @return void
 */
function dsa_commerce_nelnet_redirect_form($form, &$form_state, $order, $payment_method) {
  $settings = array();
  
  watchdog('dsa_commerce_nelnet', 'Redirecting order: %order_id to Nelnet.', array('%order_id'=>$order->order_id));
  
  return dsa_commerce_nelnet_order_form($form, $form_state, $order, $payment_method['settings'] + $settings);
}

/**
 * dsa_commerce_nelnet_order_form function.
 * 
 * @access public
 * @param mixed $form
 * @param mixed &$form_state
 * @param mixed $order  The fully loaded order being paid for.
 * @param mixed $settings  An array of settings used to build out the form
 * @return array  A renderable form array.
 */
function dsa_commerce_nelnet_order_form($form, &$form_state, $order, $settings) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $amount = $wrapper->commerce_order_total->amount->value();
  $billing_address = $wrapper->commerce_customer_billing->commerce_customer_address->value();
    
  $request=new DSA_Commerce_Nelnet_Request();
  
  $request->addParameter('orderNumber',$order->order_id);
  $request->addParameter('orderType',$settings['commerce_nelnet_order_type']);
  $request->addParameter('orderDescription',$settings['commerce_nelnet_order_description']);
  $request->addParameter('amountDue',$amount);
  $request->addParameter('paymentMethod','cc');
  $request->addParameter('streetOne',$billing_address['thoroughfare']);
  $request->addParameter('city',$billing_address['locality']);
  $request->addParameter('state',$billing_address['administrative_area']);
  $request->addParameter('zip',$billing_address['postal_code']);
  
  // Sometimes the e-mail will be on the profile if we are using anonymous checkout,
  //  otherwise the order gets the one from the user object
  $customer_profile_fields=$wrapper->commerce_customer_billing->getPropertyInfo();
  if (array_key_exists('field_email',$customer_profile_fields)) {
    $request->addParameter('email',$wrapper->commerce_customer_billing->field_email->value());
  } else {
    $request->addParameter('email',$order->mail);
  }
  
  $request->addParameter('redirectUrl',url('checkout/nelnet/return', array('absolute' => TRUE)));
  $request->addParameter('redirectUrlParameters','orderNumber,transactionType,transactionStatus,transactionTotalAmount,transactionId,transactionResultCode,transactionAcountType,transactionResultMessage');
  $request->addParameter('timestamp',$request->getTimestamp());
  $request->addParameter('hash',$request->generateHash($settings['commerce_nelnet_key'],$settings['commerce_nelnet_hash_type']));
  
  $request->addParametersToForm($form);
  
  $mode=$settings['commerce_nelnet_debug'];
  if ($mode==0)
    $action=$settings['commerce_nelnet_test_url'];
  else
    $action=$settings['commerce_nelnet_live_url'];
  $form['#action']=$action;
  $form['#method']='GET';
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Complete Purchase at Nelnet'),
  );
  
  unset($form['#token'], $form['form_build_id'], $form['#build_id']);

  return $form;
}

/**
 * dsa_commerce_nelnet_order_return function.  Callback for when user returns from nelnet
 * 
 * @access public
 * @return void
 */
function dsa_commerce_nelnet_order_return() {
  $order_id = dsa_commerce_nelnet_get_value('orderNumber',TRUE);
  $order_status = dsa_commerce_nelnet_get_value('transactionStatus');
  
  watchdog('dsa_commerce_nelnet', 'Receiving new order notification for order %order_id with status %order_status.', array('%order_id'=>$order_id,'%order_status'=>$order_status));
  
  if ($order_id>0)
    $order = commerce_order_load($order_id);
  
  if (is_object($order))
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  
  // Ensure we could load the order and that it was in the right step
  if (!is_object($order) || $order_wrapper->status->value() != 'checkout_payment') {
    drupal_set_message(t('There was a problem loading your order, please check your cart and try again.'), 'warning');
    
    drupal_goto('cart');
    return FALSE;
  }
  
  $qp_payment = $order_wrapper->commerce_order_total->value();
  
  $trans = commerce_payment_transaction_new('commerce_payment_nelnet', $order->order_id);
  $trans->currency_code = $qp_payment['currency_code'];
  $trans->instance_id = $order->data['payment_method'];
  $trans->amount = dsa_commerce_nelnet_get_value('transactionTotalAmount');
  
  // Validate payment
  $result = FALSE;
  $transactionType = dsa_commerce_nelnet_get_value('transactionType',TRUE);
  $transactionStatus = dsa_commerce_nelnet_get_value('transactionStatus',TRUE);
  switch ($transactionType) {
    case 1: // CC
    case 2:
      $result = in_array($transactionStatus,array(1));
      break;
    case 3: // eCheck
      $result = in_array($transactionStatus,array(5,6,8));
      break;
  }
  
  if ($result && dsa_commerce_nelnet_get_value('transactionTotalAmount',FALSE,TRUE)>0) { // Successful payment
    $trans->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $trans->message = nl2br( 
"Nelnet Success
Confirmation Number: @confnum
Authorization: @auth
Payment Type: @account
Message: @msg");
    $trans->message_variables['@confnum'] = dsa_commerce_nelnet_get_value('transactionId');
    $trans->message_variables['@auth'] = dsa_commerce_nelnet_get_value('transactionResultCode');
    $trans->message_variables['@account'] = dsa_commerce_nelnet_get_value('transactionAcountType');
    $trans->message_variables['@msg'] = dsa_commerce_nelnet_get_value('transactionResultMessage');
    
	  commerce_payment_transaction_save($trans);
	  
	  watchdog('dsa_commerce_nelnet', '%order_id return from Nelnet succeeded and marked as pending.', array('%order_id'=>$order->order_id));
    
    commerce_payment_redirect_pane_next_page($order);
  } else {
  	$trans->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $trans->message = nl2br(
"Nelnet Failure
Message: @msg");
    $trans->message_variables = array('@msg' => dsa_commerce_nelnet_get_value('transactionResultMessage'));
    
    commerce_payment_transaction_save($trans);
    
    // Give message to user here…
    $failureMessage=dsa_commerce_nelnet_friendly_error_message($transactionStatus);
    
    drupal_set_message(t($failureMessage),'warning');
    
    watchdog('dsa_commerce_nelnet', '%order_id return from Nelnet as failed.', array('%order_id'=>$order->order_id));
    
    commerce_payment_redirect_pane_previous_page($order);
  }
  
  drupal_goto('checkout/'.$order->order_id);
}

/**
 * dsa_commerce_nelnet_friendly_error_message function.  Show an error message for a transaction status
 * 
 * @access public
 * @param int $error  Possible nelnet transaction status
 * @return void
 */
function dsa_commerce_nelnet_friendly_error_message($error) {
  $errors=array(
    1=>'Your payment was successful, thank you.',
    2=>'Our payment service reports your credit card has been declined. Please review your checkout information for possible errors and try again.',
    3=>'Our payment service reports your credit card has an error. Please review your checkout information for possible errors and try again.',
    4=>'Our payment service reports a miscellaneous payment error. Please review your checkout information for possible errors and try again.',
    5=>'Your payment was successful, thank you.',
    6=>'Your payment was successful, thank you.',
    7=>'Our payment service reports your eCheck has an error. Please review your checkout information for possible errors and try again.',
    8=>'Your payment was successful, thank you.');
  
  if (array_key_exists($error,$errors))
  	return $errors[$error];
  else
  	return $errors[4];
}

/**
 * dsa_commerce_nelnet_get_value function.  Parse return values from nelnet
 * 
 * @access public
 * @param mixed $key
 * @param mixed $asInteger (default: FALSE)
 * @param mixed $asFloat (default: FALSE)
 * @return void
 */
function dsa_commerce_nelnet_get_value($key,$asInteger=FALSE,$asFloat=FALSE) {
  if (array_key_exists($key,$_GET)) {
    $value = check_plain($_GET[$key]);
    if ($asInteger)
      $value = intval($value);
    if ($asFloat)
      $value = floatval($value);
    return $value;
  } else {
    return FALSE;
  }
}
