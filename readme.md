## About
This is a Nelnet integration module for Drupal 7. It was created for [Student Life at the University of Michigan](https://studentlife.umich.edu/). You will want to download the files and install them in your modules directory like any other drupal module. Be sure to have Drupal Commerce installed before enabling. You can read more about [Drupal Commerce Payment Method Modules here](https://docs.drupalcommerce.org/commerce1/developer-guide/utilizing-core-apis/writing-a-payment-method-module). This module is an off-site payment method.

## Installation and Configuring

You can go to your site�s modules page to enable the module. Once enabled it will add a payment rule like any other Drupal Commerce Payment method. This location is fairly deep. You will want to go to Configuration -> Workflow -> Rules and Edit the Nelnet rule. Then you want to edit the enable payment method: Nelnet action.

You will now be on a page where you can enter the Nelnet parameters that were given to you when you set up your account.

Shared Secret Key: This will be given to you when setting up your account

Test URL: If your Nelnet instance has not been moved to production yet you can enter the payment URL they gave you here.

Live URL: Once your instance is moved to production they will give you another URL that you can enter here.

Debug Mode: Select whether you want to be on the production or test instance.

Hashing Method: Nelnet�s documentation will tell you what method to use, I have never seen anything other than MD5, but SHA256 is available if they ever switch in the future.

Order Type: This is used to correlate orders on Nelnet�s end and they will give you this.

Order Description: You can enter anything here you want to show to the end user on the Nelnet checkout page.

Once you have these parameters set up the module will work like any other drupal commerce payment method. You can add conditions to the rule to enable it for only certain use cases if you wish. Remember, this module only works with the Nelnet payment gateway. If you need to issue refunds you need to do that through the Nelnet interface.